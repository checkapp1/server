const {
  ObjectId
} = require('mongodb');

const HIV = {
  name: 'HIV',
  actions: [{
    _id: ObjectId(),
    name: 'Exame de Sangue',
    wikiID: '1231231',
    targetProfiles: [{
        sex: 'masculino',
        minAge: 15,
        maxAge: 65,
      },
      {
        sex: 'feminino',
        minAge: 15,
        maxAge: 65,
      },
    ],
  }, ],
};

const osteo = {
  name: 'Osteoporose',
  actions: [{
      _id: ObjectId(),
      name: 'Questionário FRAX',
      formName: 'frax_form',
      targetProfiles: [{
        sex: 'feminino',
        minAge: 45,
        maxAge: 200,
      }, ],
    },
    {
      _id: ObjectId(),
      name: 'Densitometria Óssea',
      disclaimer: 'fazer FRAX antes',
      wikiID: '123123',
      targetProfiles: [{
        sex: 'feminino',
        minAge: 55,
        maxAge: 200,
      }, ],
    },
  ],
};

const sifilis = {
  name: 'Sífilis',
  actions: [{
    _id: ObjectId(),
    name: 'Exame de Sangue',
    wikiID: '1231231',
    targetProfiles: [{
        sex: 'masculino',
        minAge: 15,
        maxAge: 65,
      },
      {
        sex: 'feminino',
        minAge: 15,
        maxAge: 65,
      },
    ],
  }],
};

const depressao = {
  name: 'Depressão',
  actions: [{
    _id: ObjectId(),
    name: 'Acompanhamento Psicológico',
    wikiID: '45454545',
    targetProfiles: [{
        sex: 'masculino',
        minAge: 15,
        maxAge: 75,
      },
      {
        sex: 'feminino',
        minAge: 15,
        maxAge: 75,
      },
    ],
  }, ],
};

const cancer_pulmao = {
  name: 'Câncer Pulmonar',
  actions: [{
    _id: ObjectId(),
    name: 'Radiografia Pulmonar',
    disclaimer: 'Verificar fatores de risco',
    wikiID: '67676767',
    targetProfiles: [{
        sex: 'masculino',
        minAge: 55,
        maxAge: 75,
      },
      {
        sex: 'feminino',
        minAge: 55,
        maxAge: 75,
      },
    ],
  }, ],
};

const allDiseases = [
  HIV,
  osteo,
  sifilis,
  cancer_pulmao,
  depressao
];

const bloodTestWiki = {
  name: "Exame de Sangue",
  tabs: [{
    name: 'General',
    body: `
    <html><body>

      <h1>Exame de Sangue</h1>

      <p><a href="https://pt.wikipedia.org/wiki/Exame_de_sangue"> Exames de sangue </a>são exames laboratoriais realizados no sangue para adquirir
      informações sobre doenças e funções dos órgãos. Como o sangue flui por todo o
      corpo, atuando como meio de obtenção de oxigênio e nutrientes pelos tecidos, e
      levando de volta os produtos usados para os sistemas excretórios, o estado do
      sangue afeta, ou é afetado, por muitas doenças ou condições médicas. Por estas
      razões, os exames de sangue são os exames médicos mais comuns. O sangue é obtido
      de um paciente através de uma punção na veia ou um pequeno furo no dedo.
      O sangue é útil por ser um meio relativamente não-invasivo se se obter células,
      fluido extracelular (plasma sanguíneo) do corpo para verificar sua saúde. </p>

    </body></html>
    `,
  },
  {
    name: 'Exames',
    body: `
    <html><body>

      <h1>Exame de Sangue</h1>

      <ul>

        <li>quatro eletrólitos</li>

        <ul>

          <li>sódio</li>
          <li>potássio</li>
          <li>cloreto</li>
          <li>bicarbonato ou CO2</li>

        </ul>

        <li>nitrogênio uréico do sangue (BUN)</li>
        <li>creatinina</li>
        <li>glicose</li>

      </ul>

    </body></html>
    `,
  },
]
};

async function seed(db) {
  const collections = (await db.collections()).reduce(
    (acc, col) => acc.concat(col.namespace.split('.')[1]),
    []
  );

  for (const colName of ['wikis', 'diseases']) {
    if (collections.includes(colName)) await db.collection(colName).drop();
  }

  const {
    insertedId
  } = await db
    .collection('wikis')
    .insertOne({ ...bloodTestWiki });

  HIV.actions[0].wikiID = insertedId;

  await db.collection('diseases').insertMany([HIV, osteo, sifilis, cancer_pulmao, depressao]);
}

module.exports = {
  seed,
  allDiseases,
};
