const {
  MongoClient: { connect },
} = require('mongodb');

const { seed } = require('./seed');

const { MONGO_URL, MONGO_DB } = process.env;

async function setupDB(db_name = MONGO_DB) {
  try {
    const client = await connect(MONGO_URL);
    const db = client.db(db_name);

    if (process.env.NODE_ENV == 'development') {
      seed(db);
    }
    return db;
  } catch (err) {
    console.log('failed to connect to mongo');
    process.exit(1);
  }
}

module.exports = {
  setupDB,
};
