const { validateWikiCreation } = require('./validateWikiCreation');

const toArray = jest.fn();

const mock = {
  collection: () => {
    return {
      find: () => {
        return {
          toArray,
        };
      },
    };
  },
};

describe('wiki creation', () => {
  it('enforces given structure', async () => {
    const wrong1 = {
      tabs: [{ name: 'foo', content: "attr name should be 'body'" }],
    };
    const wrong2 = {
      content: [{ name: 'foo', body: 'right attribute name' }],
    };
    const wrong3 = {
      tabs: [{ name: 'foo', body: 'right attribute name' }],
    };
    const right = {
      name: "Valid name",
      tabs: [{ name: "tab", body: "tab content" }]
    };

    const wrongValidation1 = await validateWikiCreation(wrong1, mock);
    const wrongValidation2 = await validateWikiCreation(wrong2, mock);
    const wrongValidation3 = await validateWikiCreation(wrong3, mock);
    const rightValidation = await validateWikiCreation(right, mock);

    expect(wrongValidation1.isValid).toBeFalsy();
    expect(wrongValidation2.isValid).toBeFalsy();
    expect(wrongValidation3.isValid).toBeFalsy();
    expect(rightValidation.isValid).toBeTruthy();
  });

  it.skip("doesn't accept empty values", async () => {
    const emptyName = [{ name: '', body: 'foo bar baz' }];
    const emptyBody = [{ name: 'foo bar baz', body: '' }];

    const wrongName = await validateWikiCreation(emptyName, mock);
    const wrongBody = await validateWikiCreation(emptyBody, mock);

    expect(wrongName.isValid).toBeFalsy();
    expect(wrongBody.isValid).toBeFalsy();
  });
});
