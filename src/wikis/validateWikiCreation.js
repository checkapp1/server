async function validateWikiCreation(candidate, db) {
  const { name, tabs } = candidate;

  if (!name) {
    return {
      isValid: false,
      reason: 'wiki must have a name'
    };
  }

  if (!tabs) {
    return {
      isValid: false,
      reason: 'wrong parameters -- expect root key "wiki"',
    };
  }

  const rightKeys = tabs.reduce(
    (acc, el) => acc && 'name' in el && 'body' in el,
    true
  );

  if (!rightKeys) {
    return {
      isValid: false,
      reason:
        'wrong parameters -- expect { tabs: [{ name: "...", body: "..."}] }',
    };
  }

  const haveEmptyAttr = tabs.some(
    (w) => w.name.length == 0 || w.body.length == 0
  );

  if (haveEmptyAttr) {
    return {
      isValid: false,
      reason: "can't have empty name or body",
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateWikiCreation,
};
