const { ObjectId } = require('mongodb');
const Router = require('koa-router');

const { validateWikiCreation } = require('./validateWikiCreation');

const wikiRouter = new Router({ prefix: '/wikis' });

wikiRouter.get('/', async (ctx, next) => {
  const { db } = ctx;

  const wikis = await db
    .collection('wikis')
    .find()
    .toArray();

  ctx.body = { wikis };
});

wikiRouter.get('/:id', async (ctx, next) => {
  const { db } = ctx;
  const _id = ObjectId(ctx.params.id);

  const wiki = await db.collection('wikis').findOne({ _id });

  if (!wiki) {
    ctx.body = {
      error: `no wiki with id ${ctx.params.id}`,
    };
    ctx.status = 404;
    return;
  }

  ctx.body = { wiki };
});

wikiRouter.post('/', async (ctx, next) => {
  const wiki = ctx.request.body;

  const { isValid, reason } = await validateWikiCreation(wiki, ctx.db);

  if (!isValid) {
    ctx.body = {
      error: reason,
    };
    ctx.status = 400;
    return;
  }

  const res = await ctx.db.collection('wikis').insertOne({ ...wiki });

  ctx.body = { wiki: res.ops[0] };
});

wikiRouter.put('/:id', async (ctx) => {
  const { db } = ctx;
  const _id = ObjectId(ctx.params.id);

  const wiki = await db.collection('wikis').findOne({ _id });

  if (!wiki) {
    ctx.body = {
      error: `no wiki with id ${ctx.params.id}`,
    };
    ctx.status = 403;
    return;
  }

  const { value } = await db.collection('wikis').findOneAndReplace(
    { _id },
    {
      _id,
      name: ctx.request.body.name ? ctx.request.body.name : wiki.name,
      tabs: ctx.request.body.tabs ? ctx.request.body.tabs : wiki.tabs,
    },
    {
      returnOriginal: false,
    }
  );

  ctx.body = { wiki: value };
});

wikiRouter.delete('/:id', async (ctx) => {
  const _id = ObjectId(ctx.params.id);

  const { value } = await ctx.db.collection('wikis').findOneAndDelete({ _id });

  if (!value) {
    ctx.body = { error: 'cannot find wiki with id ' + ctx.params.id };
    ctx.status = 403;
    return;
  }

  ctx.body = { wiki: value };
});

module.exports = {
  wikiRouter,
};
