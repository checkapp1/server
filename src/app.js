const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');

const { diseasesRouter } = require('./diseases');
const { profilingRouter } = require('./profiling');
const { wikiRouter } = require('./wikis');
const { authRouter } = require('./auth');

const app = new Koa();

app.use(bodyParser());
app.use(cors());

app.use(diseasesRouter.routes());
app.use(diseasesRouter.allowedMethods());

app.use(profilingRouter.routes());
app.use(profilingRouter.allowedMethods());

app.use(wikiRouter.routes());
app.use(wikiRouter.allowedMethods());

app.use(authRouter.routes());
app.use(authRouter.allowedMethods());

module.exports = {
  app,
};
