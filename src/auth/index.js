const Router = require('koa-router');

const { validateRegistration } = require('./validateRegistration');
const { validateLogin } = require('./validateLogin');

const saltRounds = 10;
const authRouter = new Router();

authRouter.post('/register', async (ctx) => {
  const { name, email, password, passwordConfirmation } = ctx.request.body;
  const { db, bcrypt } = ctx;

  const { isValid, reason } = await validateRegistration(
    name,
    email,
    password,
    passwordConfirmation,
    db
  );

  if (!isValid) {
    ctx.body = { error: reason };
    return;
  }

  const hash = await bcrypt.hash(password, saltRounds);

  const { ops } = await db.collection('users').insertOne({
    name,
    email,
    password: hash,
  });

  const user = { ...ops[0] };
  delete user.password;
  ctx.body = { user };
});

authRouter.post('/login', async (ctx) => {
  const { db, bcrypt, request } = ctx;

  const { isValid, reason } = await validateLogin(request.body, db, bcrypt);

  if (!isValid) {
    ctx.body = { error: reason };
    ctx.status = 403;
    return;
  }

  const token = 'super.secret.token';

  ctx.body = { token };
});

module.exports = {
  authRouter,
};
