async function validateRegistration(
  name,
  email,
  password,
  passwordConfirmation,
  db
) {
  if (!name) {
    return {
      isValid: false,
      reason: 'must suply a name',
    };
  }

  if (!email) {
    return {
      isValid: false,
      reason: 'must suply a email',
    };
  }

  if (!password) {
    return {
      isValid: false,
      reason: 'must suply a password',
    };
  }

  if (!passwordConfirmation) {
    return {
      isValid: false,
      reason: 'must suply a passwordConfirmation',
    };
  }

  if (password != passwordConfirmation) {
    return {
      isValid: false,
      reason: 'password and confirmation mismatch',
    };
  }

  const user = await db.collection('users').findOne({
    $or: [{ name }, { email }],
  });

  if (user) {
    return {
      isValid: false,
      reason: 'name or email already in use',
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateRegistration,
};
