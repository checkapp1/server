const name = 'User Name';
const mail = 'user@mail.com';
const pass = '12345678';

function noName() {
  return {
    email: mail,
    password: pass,
    passwordConfirmation: pass,
  };
}

function noMail() {
  return {
    name,
    password: pass,
    passwordConfirmation: pass,
  };
}

function noPass() {
  return {
    name,
    email: mail,
  };
}

function right() {
  return {
    name,
    email: mail,
    password: pass,
    passwordConfirmation: pass,
  };
}

module.exports = {
  noName,
  noMail,
  noPass,
  right,
};
