async function validateLogin({ email, password }, db, bcrypt) {
  const user = await db.collection('users').findOne({ email });

  const reason = 'email or password do not match';

  if (!user) {
    return {
      isValid: false,
      reason,
    };
  }

  const pwMatch = await bcrypt.compare(password, user.password);

  if (!pwMatch) {
    return {
      isValid: false,
      reason,
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateLogin,
};
