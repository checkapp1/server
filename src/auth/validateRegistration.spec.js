const { validateRegistration } = require('./validateRegistration');

const mockQuery = {
  findOne: jest.fn(),
};

const mockDB = {
  collection: jest.fn(() => mockQuery),
};

describe('validateRegistration', () => {
  it('fails without name', async () => {
    const { isValid } = await validateRegistration(
      undefined,
      'user@mail.com',
      '12345678',
      '12345678',
      mockDB
    );

    expect(isValid).toBeFalsy();
  });
});
