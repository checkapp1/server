const { ObjectId } = require('mongodb');
const Router = require('koa-router');
const diseasesRouter = new Router({ prefix: '/diseases' });

const { validateDiseaseCreation } = require('./validateDiseaseCreation');
const { validateDiseaseUpdate } = require('./validateDiseaseUpdate');
const { validateActionCreation } = require('./validateActionCreation');
const { validateActionUpdate } = require('./validateActionUpdate');
const { validateActionDeletion } = require('./validateActionDeletion');

// Get all diseases

diseasesRouter.get('/', async (ctx, next) => {
  const res = await ctx.db
    .collection('diseases')
    .find()
    .toArray();

  ctx.body = { diseases: res };
});

// Disease related routes

diseasesRouter.get('/:id', async (ctx, next) => {
  const { id } = ctx.params;
  const res = await ctx.db
    .collection('diseases')
    .findOne({ _id: ObjectId(id) });

  if (res == null) {
    ctx.body = {
      error: 'Disease not found.',
    };
    ctx.status = 404;
    return;
  }
  ctx.body = { disease: res };
});

diseasesRouter.post('/', async (ctx, next) => {
  const { name } = ctx.request.body;

  const { isValid, reason } = await validateDiseaseCreation(name, ctx.db);

  if (!isValid) {
    ctx.body = {
      error: reason,
    };
    ctx.status = 400;
    return;
  }

  const res = await ctx.db
    .collection('diseases')
    .insertOne({ name, actions: [] });

  ctx.body = { disease: res.ops[0] };
});

diseasesRouter.put('/:id', async (ctx, next) => {
  const { id } = ctx.params;
  const { name } = ctx.request.body;

  const { isValid, reason, status } = await validateDiseaseUpdate(
    { _id: id, name },
    ctx.db
  );

  if (!isValid) {
    ctx.body = {
      error: reason,
      status,
    };
    return;
  }

  const res = await ctx.db.collection('diseases').findOneAndUpdate(
    { _id: ObjectId(id) },
    {
      $set: { name },
    },
    {
      returnOriginal: false,
    }
  );

  ctx.body = { disease: res.value };
});

diseasesRouter.delete('/:id', async (ctx, next) => {
  const { id } = ctx.params;

  const res = await ctx.db
    .collection('diseases')
    .findOneAndDelete({ _id: ObjectId(id) });

  if (res.value == null) {
    ctx.body = {
      error: 'Disease not found.',
    };
    ctx.status = 404;
    return;
  }

  ctx.body = { disease: res.value };
});

// Action related routes

diseasesRouter.get('/:diseaseID/actions/actionID', async (ctx, next) => {
  const { diseaseID, actionID } = ctx.params;
  const res = await ctx.db
    .collection('diseases')
    .findOne({ _id: ObjectId(diseaseID) });

  if (res == null) {
    ctx.body = {
      error: 'Disease not found.',
    };
    ctx.status = 404;
    return;
  }

  const action = disease.actions.reduce((acc, curr) => {
    return curr._id == actionID ? curr : acc;
  }, undefined);

  if (!action) {
    ctx.body = {
      error: 'Action not found.',
    };
    ctx.status = 404;
    return;
  }

  ctx.body = { action };
});

diseasesRouter.post('/:id/actions/', async (ctx, next) => {
  const { id } = ctx.params;
  const {
    name,
    disclaimer,
    wikiID,
    formName,
    targetProfiles,
  } = ctx.request.body;
  const _id = ObjectId(id);

  const { isValid, reason, status } = await validateActionCreation(
    id,
    { name, disclaimer, wikiID, formName, targetProfiles },
    ctx.db
  );

  if (!isValid) {
    ctx.body = {
      error: reason,
      status,
    };
    return;
  }

  const res = await ctx.db.collection('diseases').findOneAndUpdate(
    { _id },
    {
      $push: {
        actions: {
          _id: ObjectId(),
          name,
          formName,
          wikiID,
          disclaimer,
          targetProfiles,
        },
      },
    },
    {
      ignoreUndefined: true,
      returnOriginal: false,
    }
  );

  ctx.body = { disease: res.value };
});

diseasesRouter.put('/:diseaseID/actions/:actionID', async (ctx, next) => {
  const { diseaseID, actionID } = ctx.params;
  const {
    name,
    disclaimer,
    wikiID,
    formName,
    targetProfiles,
  } = ctx.request.body;

  const { isValid, reason, status } = await validateActionUpdate(
    diseaseID,
    actionID,
    { name, disclaimer, wikiID, formName, targetProfiles },
    ctx.db
  );

  if (!isValid) {
    ctx.body = {
      error: reason,
      status,
    };
    return;
  }

  const res = await ctx.db.collection('diseases').findOneAndUpdate(
    {
      $and: [
        { _id: ObjectId(diseaseID) },
        { 'actions._id': ObjectId(actionID) },
      ],
    },
    {
      $set: {
        'actions.$': {
          _id: ObjectId(actionID),
          name,
          formName,
          wikiID,
          disclaimer,
          targetProfiles,
        },
      },
    },
    {
      ignoreUndefined: true,
      returnOriginal: false,
    }
  );

  ctx.body = { disease: res.value };
});

diseasesRouter.delete('/:diseaseID/actions/:actionID', async (ctx, next) => {
  const { diseaseID, actionID } = ctx.params;
  const { db } = ctx;

  const disease = await db
    .collection('diseases')
    .findOne({ _id: ObjectId(diseaseID) });

  if (!disease) {
    ctx.body = {
      error: `disease ${diseaseID} not found`,
    };
    ctx.status = 403;
    return;
  }

  const newActions = disease.actions.filter((action) => action._id != actionID);

  const res = await db
    .collection('diseases')
    .findOneAndUpdate(
      { _id: ObjectId(diseaseID) },
      { $set: { actions: newActions } },
      { returnOriginal: false }
    );

  console.log(res);

  ctx.body = { disease: res.value };
});

module.exports = {
  diseasesRouter,
};
