const { validateDiseaseUpdate } = require('./validateDiseaseUpdate');
const { setupDB } = require('../../src/db/index');

test('it is defined', () => {
    expect(validateDiseaseUpdate).toBeDefined();
});

describe('validation without db', () => {

    it('rejects inputs with wrong formats', async () => {
        const noName = await validateDiseaseUpdate({ _id: '1' }, {});
        const noId = await validateDiseaseUpdate({ name: 'x' }, {});

        expect(noName.isValid).toBeFalsy();
        expect(noId.isValid).toBeFalsy();
    });

    it('rejects wrong name formats', async () => {
        const wrongName = await validateDiseaseUpdate({ name: '123 @#' }, {});

        expect(wrongName.isValid).toBeFalsy();
    });

});

describe('validation with db', () => {
    let db;

    beforeAll(async () => {
        db = await setupDB('test');
    });

    afterAll(async () => {
        await db.close();
    });

    beforeEach(async () => {
        const { insertedId } = await db.collection('diseases').insertOne({
            name: "teste"
        });
        diseaseID = insertedId;
    });

    afterEach(async () => {
        await db.collection('diseases').drop();
    });

    it('aceita nomes com acento', async () => {
        const { isValid, reason } = await validateDiseaseUpdate({ name: 'Câncer de Pulmão', _id: diseaseID }, db);
        expect(isValid).toBeTruthy();
    });

    it('rejects id not in db', async () => {
        const wrongId = await validateDiseaseUpdate({ name: 'nome', _id: `FF690AFF690A` }, db);

        expect(wrongId.isValid).toBeFalsy();
    })


    it('accepts valid id', async () => {
        const wrongId = await validateDiseaseUpdate({ name: 'nome', _id: diseaseID }, db);

        expect(wrongId.isValid).toBeTruthy();
    })
});