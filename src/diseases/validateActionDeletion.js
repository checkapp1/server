const { ObjectId } = require('mongodb');

async function validateActionDeletion(diseaseID, actionID, db) {
  if (!diseaseID) {
    return {
      isValid: false,
      reason: 'must provide disease ID',
      status: 400
    };
  }
  if (!actionID) {
    return {
      isValid: false,
      reason: 'must provide action ID',
      status: 400
    };
  }

  const disease = await db.collection('diseases').findOne({ _id: ObjectId(diseaseID) });

  if (!disease) {
    return {
      isValid: false,
      reason: 'unknown disease with provided ID',
      status: 404
    };
  }

  const ActionExists = disease.actions.some((a) => (a._id.toString() == actionID));

  if (!ActionExists) {
    return {
      isValid: false,
      reason: `disease ${disease.name} doesn't have action with provided id`,
      status: 404
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateActionDeletion,
}