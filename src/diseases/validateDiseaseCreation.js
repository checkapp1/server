async function validateDiseaseCreation(name, db) {
  if (name == null) {
    return {
      isValid: false,
      reason: 'wrong parameters -- expect "name"',
    };
  }
  if (!name.match(/^[\w\sÀ-ú\-]{3,50}$/i)) {
    return {
      isValid: false,
      reason: 'name format mismatch',
    };
  } else {
    const obj = await db
      .collection('diseases')
      .find({ name })
      .toArray();

    if (obj.length > 0) {
      return {
        isValid: false,
        reason: 'disease already exists',
      };
    } else {
      return {
        isValid: true,
      };
    }
  }
}

module.exports = {
  validateDiseaseCreation,
};
