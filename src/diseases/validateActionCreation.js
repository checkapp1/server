const { ObjectId } = require('mongodb');
const { validateAction } = require('./actions/validateAction');

async function validateActionCreation(diseaseID, action, db) {
  if (!diseaseID) {
    return {
      isValid: false,
      reason: 'must provide disease ID',
      status: 400
    };
  }

  const validAction = await validateAction(action);

  if (!validAction.isValid) {
    return {
      isValid: false,
      reason: validAction.reason,
      status: 400
    };
  }

  const disease = await db.collection('diseases').findOne({ _id: ObjectId(diseaseID) });

  if (!disease) {
    return {
      isValid: false,
      reason: 'unknown disease with provided ID',
      status: 404
    };
  }

  const isNameDuplicate = disease.actions.some((a) => a.name === action.name);

  if (isNameDuplicate) {
    return {
      isValid: false,
      reason: `disease ${disease.name} already has action ${action.name}`,
      status: 400
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateActionCreation,
};
