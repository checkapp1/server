const { validateActionDeletion } = require('./validateActionDeletion');
const { ObjectID } = require('mongodb');

const mockQuery = {
  findOne: jest.fn(),
};

const mockDB = {
  collection: jest.fn(() => mockQuery),
};

test('it is defined', () => {
  expect(validateActionDeletion).toBeDefined();
});

test('fails without params', async () => {
  const noDiseaseID = await validateActionDeletion(null, {}, {});
  const noActionID = validateActionDeletion('12345', null, {});
  expect(noDiseaseID.isValid).toBeFalsy();
  expect(noActionID.isValid).toBeFalsy();
});

it('fails with wrong action id', async () => {
  mockQuery.findOne.mockReturnValueOnce({
    name: 'disease',
    actions: [
      {
        _id: `123123123123`,
        name: 'action',
        formName: 'not the same',
        targetProfiles: [
          {
            sex: 'masculino',
            minAge: 55,
            maxAge: 75,
          },
        ],
      },
    ],
  });

  const { isValid, reason } = await validateActionDeletion(
    `123456123456`,
    '444',
    mockDB
  );

  expect(isValid).toBeFalsy();
});
