const { validateDiseaseCreation } = require('./validateDiseaseCreation');

test('it is defined', () => {
  expect(validateDiseaseCreation).toBeDefined();
});

describe('disease creation', () => {
  const find = jest.fn();

  const mock = {
    collection: () => {
      return { find };
    },
  };

  it('rejects names shorter than 3 or longer than 50', async () => {
    const tooShort = await validateDiseaseCreation('ab', {});
    const tooLong = await validateDiseaseCreation('a'.repeat(51), {});

    expect(tooShort.isValid).toBeFalsy();
    expect(tooLong.isValid).toBeFalsy();
  });

  it('aceita nomes com acento', async () => {
    find.mockReturnValueOnce({
      toArray: jest.fn().mockResolvedValueOnce([]),
    });

    const { isValid } = await validateDiseaseCreation('Câncer de Pulmão', mock);

    expect(isValid).toBeTruthy();
  });

  it('accepts names with spaces', async () => {
    find.mockReturnValueOnce({
      toArray: jest.fn().mockResolvedValueOnce([]),
    });

    const { isValid } = await validateDiseaseCreation('name with spaces', mock);

    expect(isValid).toBeTruthy();
  });

  it('accepts name with dashes', async () => {
    find.mockReturnValueOnce({
      toArray: jest.fn().mockResolvedValueOnce([]),
    });

    const { isValid } = await validateDiseaseCreation('co-vid', mock);

    expect(isValid).toBeTruthy();
  });

  it('accepts names with numbers', async () => {
    find.mockReturnValueOnce({
      toArray: jest.fn().mockResolvedValueOnce([]),
    });

    const { isValid } = await validateDiseaseCreation('covid-19', mock);

    expect(isValid).toBeTruthy();
  });

  it('rejects repetitions', async () => {
    find.mockReturnValueOnce({
      toArray: jest.fn().mockResolvedValueOnce([{ something: 'foo' }]),
    });

    const { isValid } = await validateDiseaseCreation('some used name', mock);

    expect(isValid).toBeFalsy();
  });
});
