const { validateActionCreation } = require('./validateActionCreation');

const mockQuery = {
  findOne: jest.fn(),
};

const mockDB = {
  collection: jest.fn(() => mockQuery),
};

test('it is defined', () => {
  expect(validateActionCreation).toBeDefined();
});

test('fails without params', async () => {
  const noDiseaseID = await validateActionCreation(null, {}, mockDB);
  const noAction = validateActionCreation('12345', null, mockDB);
  expect(noDiseaseID.isValid).toBeFalsy();
  expect(noAction.isValid).toBeFalsy();
});

test('fails with invalid diseaseID', async () => {
  const id = '1231090913';

  mockQuery.findOne.mockReturnValueOnce(null);

  const { isValid } = await validateActionCreation(
    id,
    {
      name: 'foo bar',
      formName: 'randomForm',
    },
    mockDB
  );

  expect(isValid).toBeFalsy();
});

test('fails when name is duplicated for disease', async () => {
  const action = {
    name: 'action',
    formName: 'name of the form',
    targetProfiles: [
      {
        sex: 'masculino',
        minAge: 55,
        maxAge: 75,
      },
      {
        sex: 'feminino',
        minAge: 55,
        maxAge: 75,
      },
    ],
  };

  mockQuery.findOne.mockReturnValueOnce({
    name: 'disease',
    actions: [
      {
        name: 'action',
        formName: 'not the same',
        targetProfiles: [
          {
            sex: 'masculino',
            minAge: 55,
            maxAge: 75,
          },
        ],
      },
    ],
  });

  const { isValid } = await validateActionCreation(
    `111213141516`,
    action,
    mockDB
  );

  expect(isValid).toBeFalsy();
});
