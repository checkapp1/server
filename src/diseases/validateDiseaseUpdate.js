const { ObjectId } = require('mongodb');

async function validateDiseaseUpdate(disease, db) {
  const { name, _id } = disease;
  if (!name) {
    return {
      isValid: false,
      reason: 'wrong parameters -- expect "name"',
      status: 404,
    };
  }
  if (!_id) {
    return {
      isValid: false,
      reason: 'wrong parameters -- expect "_id"',
      status: 404,
    };
  }
  if (!name.match(/^[\w\sÀ-ú\-]{3,50}$/i)) {
    return {
      isValid: false,
      reason: 'name format mismatch',
      status: 404,
    };
  }

  const obj = await db
    .collection('diseases')
    .find({ _id: ObjectId(_id) })
    .toArray();

  if (obj.length == 0) {
    return {
      isValid: false,
      reason: 'no disease with sent id',
      status: 400,
    };
  }

  return {
    isValid: true,
  };
}

module.exports = {
  validateDiseaseUpdate,
};
