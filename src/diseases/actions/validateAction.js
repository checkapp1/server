async function validateAction(action) {
    if (!action) {
        return {
            isValid: false,
            reason: 'action must be defined'
        };
    }

    if (!action.name || !action.name.length) {
        return {
            isValid: false,
            reason: 'name must be defined',
        };
    }

    if ((!action.formName && !action.wikiID) || (action.formName && action.wikiID)) {
        return {
            isValid: false,
            reason: 'action must have wiki or form (but not both)',
        };
    }

    if (!action.targetProfiles || action.targetProfiles.length === 0) {
        return {
            isValid: false,
            reason: 'action must have at least one target profile',
        };
    }

    const { validProfile, reason } = await validateTargetProfiles(action.targetProfiles);

    if (!validProfile) {
        return {
            isValid: false,
            reason: reason
        };
    }

    return {
        isValid: true
    };
}

async function validateTargetProfiles(targetProfiles) {

    let validProfile;

    validProfile = targetProfiles.reduce((bool, profile) => {
        return bool && profile.sex
            && profile.sex.match('s*(masculino|feminino)s*');
    }, true);

    if (!validProfile) {
        return {
            validProfile: false,
            reason: 'sex must be either "masculino" or "feminino" in all target profiles'
        };
    };


    validProfile = targetProfiles.reduce((bool, profile) => {
        return bool
            && !(!profile.minAge || !profile.maxAge);
    }, true);

    if (!validProfile) {
        return {
            validProfile,
            reason: 'minAge and maxAge must be defined in all target profiles'
        };
    };

    validProfile = targetProfiles.reduce((bool, profile) => {
        maxAge = parseInt(profile.maxAge, 10);
        minAge = parseInt(profile.minAge, 10);
        return bool
            && maxAge < 200
            && minAge > 0
            && minAge < maxAge;
    }, true);

    if (!validProfile) {
        return {
            validProfile: false,
            reason: '0 < minAge < maxAge < 200 must be true in all target profiles'
        };
    };

    return {
        validProfile: true,
    };
}

module.exports = {
    validateAction
};