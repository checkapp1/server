const { validateAction } = require('./validateAction');

test('it is defined', async () => {
    expect(validateAction).toBeDefined();
});

test('fails with null arg', async () => {
    const nullArg = await validateAction(null);
    const undefArg = await validateAction(undefined);

    expect(nullArg.isValid).toBeFalsy();
    expect(undefArg.isValid).toBeFalsy();
});

test('fails with invalid names', async () => {
    const base = {
        disclaimer: '',
        wikiID: '',
        targetProfiles: [
            {
                sex: 'masculino',
                minAge: '35',
                maxAge: '70'
            },
        ],
    };

    const emptyName = await validateAction(
        {
            name: '',
            ...base,
        }
    );

    const nullName = await validateAction(
        {
            name: null,
            ...base,
        }
    );

    const noName = await validateAction({ ...base });

    expect(emptyName.isValid).toBeFalsy();
    expect(nullName.isValid).toBeFalsy();
    expect(noName.isValid).toBeFalsy();
});

test('fails if both wikiID and formName are provided', async () => {
    const action = {
        name: 'something',
        formName: 'name of the form',
        wikiID: '1431',
        targetProfiles: [
            {
                sex: 'masculino',
                minAge: '35',
                maxAge: '70'
            },
        ],
    };

    const { isValid } = await validateAction(action);

    expect(isValid).toBeFalsy();
});

test('fails if none of wikiID and formName is provided', async () => {
    const action = {
        name: 'something',
        targetProfiles: [
            {
                sex: 'masculino',
                minAge: '35',
                maxAge: '70'
            },
        ],
    };

    const { isValid } = await validateAction(action);

    expect(isValid).toBeFalsy();
});

test('fails if no targetProfile is provided', async () => {
    const action = {
        name: 'something',
        wikiID: '13412',
    };

    const { isValid } = await validateAction(action);

    expect(isValid).toBeFalsy();
});

test('fails with invalid targetProfile', async () => {
    const noMinAge = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    maxAge: '35'
                },
            ],
        });

    const noMaxAge = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    minAge: '35'
                },
            ],
        });

    const noSex = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    maxAge: '65',
                    minAge: '25'
                },
            ],
        });

    const negMinAge = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    maxAge: '35',
                    minAge: '-12'
                },
            ],
        });

    const bigMaxAge = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    minAge: '35',
                    maxAge: '220'
                },
            ],
        });

    const MinGreaterThanMaxAge = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    maxAge: '35',
                    minAge: '65'
                },
            ],
        });

    const wrongSex = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'erro',
                    maxAge: '65',
                    minAge: '25'
                },
            ],
        });

    expect(noMinAge.isValid).toBeFalsy();
    expect(noMaxAge.isValid).toBeFalsy();
    expect(noSex.isValid).toBeFalsy();
    expect(negMinAge.isValid).toBeFalsy();
    expect(bigMaxAge.isValid).toBeFalsy();
    expect(MinGreaterThanMaxAge.isValid).toBeFalsy();
    expect(wrongSex.isValid).toBeFalsy();
});

test('suceeds with correct params', async () => {
    const validAction = await validateAction(
        {
            name: 'nome',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    maxAge: '75',
                    minAge: '25'
                },
            ],
        });

    const validActionLatin = await validateAction(
        {
            name: 'Câncer de Pulmão',
            wikiID: '12345',
            targetProfiles: [
                {
                    sex: 'masculino',
                    maxAge: '75',
                    minAge: '25'
                },
            ],
        });

    expect(validAction.isValid).toBeTruthy();
    expect(validActionLatin.isValid).toBeTruthy();
});
