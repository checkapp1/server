const { validateActionUpdate } = require('./validateActionUpdate');
const { setupDB } = require('../../src/db/index');
const { ObjectId } = require('mongodb');

const mockQuery = {
  findOne: jest.fn(),
};

const mockDB = {
  collection: jest.fn(() => mockQuery),
};

test('it is defined', () => {
  expect(validateActionUpdate).toBeDefined();
});

test('fails without params', async () => {
  const noDiseaseID = await validateActionUpdate(null, null, {}, {});
  const noActionID = await validateActionUpdate('12345', null, {}, {});
  const noAction = validateActionUpdate('12345', '54321', null, {});

  expect(noDiseaseID.isValid).toBeFalsy();
  expect(noActionID.isValid).toBeFalsy();
  expect(noAction.isValid).toBeFalsy();
});

test('fails with wrong type of action', async () => {
  mockQuery.findOne.mockReturnValueOnce({
    actions: [
      {
        _id: `123123123123`,
        name: 'action',
        formName: 'name of the form',
        targetProfiles: [
          {
            sex: 'masculino',
            minAge: 55,
            maxAge: 75,
          },
          {
            sex: 'feminino',
            minAge: 55,
            maxAge: 75,
          },
        ],
      },
    ],
  });

  const { isValid } = await validateActionUpdate(
    `123123123123`,
    `123123123123`,
    {
      name: 'action',
      wikiID: `123123123123`,
      targetProfiles: [
        {
          sex: 'feminino',
          minAge: '55',
          maxAge: '75',
        },
      ],
    },
    mockDB
  );

  expect(isValid).toBeFalsy();
});
