const { ObjectId } = require('mongodb');
const { validateAction } = require('./actions/validateAction');

async function validateActionUpdate(diseaseID, actionID, action, db) {
    if (!diseaseID) {
        return {
            isValid: false,
            reason: 'must provide disease ID',
            status: 400
        };
    }

    if (!actionID) {
        return {
            isValid: false,
            reason: 'must provide action ID',
            status: 400
        };
    }


    const validAction = await validateAction(action);

    if (!validAction.isValid) {
        return {
            isValid: false,
            reason: validAction.reason,
            status: 400
        };
    }

    const disease = await db.collection('diseases').findOne({ _id: ObjectId(diseaseID) });

    if (!disease) {
        return {
            isValid: false,
            reason: 'unknown disease with provided ID',
            status: 404
        };
    }

    const origAction = disease.actions.reduce(
        (acc, curr) => {
            return curr._id == actionID ? curr : acc;
        }, undefined);

    if (!origAction) {
        return {
            isValid: false,
            reason: `disease ${disease.name} does not have action with id provided`,
            status: 404
        };
    }

    if ((action.formName && !origAction.formName) || (action.wikiID && !origAction.wikiID)) {
        return {
            isValid: false,
            reason: `action ${origAction.name} has type ${(origAction.formName ? 'form' : 'wiki')} and has to be updated as such`,
            status: 400
        };
    }

    return {
        isValid: true,
    };
}

module.exports = {
    validateActionUpdate
};