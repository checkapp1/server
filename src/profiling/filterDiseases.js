const filterDiseases = (all, sex, age) =>
  all.filter((di) =>
    di.actions.some((act) =>
      act.targetProfiles.some(
        (prf) => sex == prf.sex && prf.minAge <= age && age <= prf.maxAge
      )
    )
  );

module.exports = {
  filterDiseases,
};
