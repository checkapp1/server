function validate(age, sex) {
  if (!Number.isInteger(age) || age < 0 || age > 120) {
    return false;
  }

  if (!(typeof sex == 'string' || sex instanceof String)) {
    return false;
  }

  if (sex.match('s*(masculino|feminino)s*')) {
    return true;
  }
  return false;
}

module.exports = {
  validate,
};
