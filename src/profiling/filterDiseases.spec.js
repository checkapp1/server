const { filterDiseases } = require('./filterDiseases');

const { allDiseases } = require('../db/seed');

const [
  HIV,
  osteo,
  sifilis,
  cancer_pulmao,
  depressao
] = allDiseases;

describe('filters correctly diseases according to profile', () => {

  test('10 yo woman should see nothing', () => {
    const filtered = filterDiseases(allDiseases, 'feminino', 10);

    expect(filtered).toEqual([]);
  });

  test('80 yo man should see nothing', () => {
    const filtered = filterDiseases(allDiseases, 'masculino', 80);

    expect(filtered).toEqual([]);
  });

  test('45 yo man should see : HIV, Sifilis, Depressão', () => {
    const filtered = filterDiseases(allDiseases, 'masculino', 45);

    expect(filtered).toContainEqual(HIV);
    expect(filtered).toContainEqual(sifilis);
    expect(filtered).toContainEqual(depressao);
    expect(filtered).not.toContainEqual(osteo);
    expect(filtered).not.toContainEqual(cancer_pulmao);
  });

  test('70 yo woman should see: osteo, Depressão, Câncer Pulmonar', () => {
    const filtered = filterDiseases(allDiseases, 'feminino', 70);

    expect(filtered).toContainEqual(osteo);
    expect(filtered).toContainEqual(depressao);
    expect(filtered).toContainEqual(cancer_pulmao);
    expect(filtered).not.toContainEqual(HIV);
    expect(filtered).not.toContainEqual(sifilis);
  });

  test('30 yo woman should see : HIV, Sifilis, Depressão', () => {
    const filtered = filterDiseases(allDiseases, 'feminino', 30);

    expect(filtered).toContainEqual(HIV);
    expect(filtered).toContainEqual(sifilis);
    expect(filtered).toContainEqual(depressao);
    expect(filtered).not.toContainEqual(osteo);
    expect(filtered).not.toContainEqual(cancer_pulmao);
  });
});
