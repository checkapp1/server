const Router = require('koa-router');
const profilingRouter = new Router();

const { validate } = require('./validate');
const { filterDiseases } = require('./filterDiseases');

// Rotas profiling
profilingRouter.get('/profiling', async (ctx, next) => {
  const sex = ctx.request.query.sex;
  const age = Number.parseInt(ctx.request.query.age);

  const areParamsValid = validate(age, sex);

  if (!areParamsValid) {
    ctx.response.status = 400;
    ctx.body = { error: 'Invalid values for age and/or sex' };

    return;
  }

  const allDiseases = await ctx.db
    .collection('diseases')
    .find()
    .toArray();

  const filteredDiseases = filterDiseases(allDiseases, sex, age);

  ctx.body = { diseases: filteredDiseases };
});

module.exports = {
  profilingRouter,
};
