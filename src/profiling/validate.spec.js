const { validate } = require('./validate');

test('valid parameters', () => {
  expect(validate(30, 'masculino')).toBeTruthy();
});

test('age < 0', () => {
  expect(validate(-2, 'feminino')).toBeFalsy();
});

test('age > 120', () => {
  expect(validate(300, 'feminino')).toBeFalsy();
});

test('age not int', () => {
  expect(validate(30.45, 'masculino')).toBeFalsy();
});

test('non valid sex', () => {
  expect(validate(45, 'maçã')).toBeFalsy();
});

test('non string sex', () => {
  expect(validate(45, 20.3)).toBeFalsy();
});

test('spaced string in sex', () => {
  expect(validate(30, ' masculino ')).toBeTruthy();
});
