const bcrypt = require('bcryptjs');
const logger = require('koa-logger');

const { setupDB } = require('./db');
const { app } = require('./app');

async function bootstrap() {
  app.use(logger());

  const db = await setupDB();
  app.context.db = db;
  app.context.bcrypt = bcrypt;
  app.listen(3000, () => console.log('\n\n\n\tServer running!\n\n\n'));
}

bootstrap();
