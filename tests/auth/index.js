const request = require('supertest');
const bcrypt = require('bcryptjs');

const { app } = require('../../src/app');
const { setupDB } = require('../../src/db');
const { noName, noMail, noPass, right } = require('../../src/auth/userFactory');

describe('sign up and sign in', () => {
  const mockRequest = request(app.callback());
  let db;

  beforeAll(async () => {
    db = await setupDB('test');
    app.context.db = db;
    app.context.bcrypt = bcrypt;
  });

  afterAll(async () => {
    await db.close();
  });

  beforeEach(async () => {
    await db.collection('users').deleteMany({});
  });

  describe('/register', () => {
    it('fails when requested without right parameters', async () => {
      const resp1 = await mockRequest.post('/register').send(noName());

      const resp2 = await mockRequest.post('/register').send(noMail());

      const resp3 = await mockRequest.post('/register').send(noPass());

      expect(resp1.error).toBeDefined();
      expect(resp2.error).toBeDefined();
      expect(resp3.error).toBeDefined();
    });

    it('returns an user when requested with correct params', async () => {
      const resp = await mockRequest.post('/register').send(right());

      const {
        body: { user },
      } = resp;

      expect(user).toBeDefined();
      expect(user.name).toEqual(right().name);
      expect(user._id).toBeDefined();
      expect(user.password).toBeUndefined();
    });
  });

  describe('/login', () => {
    it('fails to login with wrong password', async () => {
      const rawUser = right();

      await mockRequest.post('/register').send(rawUser);

      const {
        body: { error, token },
      } = await mockRequest.post('/login').send({
        email: rawUser.email,
        password: 'a' + rawUser.password,
      });

      expect(error).toBeDefined();
      expect(token).toBeUndefined();
    });

    it('fails to login with wrong email', async () => {
      const rawUser = right();

      await mockRequest.post('/register').send(rawUser);

      const {
        body: { error, token },
      } = await mockRequest.post('/login').send({
        email: 'a' + rawUser.email,
        password: rawUser.password,
      });

      expect(error).toBeDefined();
      expect(token).toBeUndefined();
    });

    it('returns a token when request /login with right credentials', async () => {
      const rawUser = right();

      await mockRequest.post('/register').send(rawUser);

      const {
        body: { error, token },
      } = await mockRequest.post('/login').send({
        email: rawUser.email,
        password: rawUser.password,
      });

      expect(error).toBeUndefined();
      expect(token).toBeDefined();
    });
  });
});
