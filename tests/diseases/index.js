const request = require('supertest');

const { app } = require('../../src/app');
const { setupDB } = require('../../src/db/index');
const { ObjectId } = require('mongodb');

describe('/diseases routes', () => {
    const mockRequest = request(app.callback());
    let db;

    beforeAll(async () => {
        db = await setupDB('test');
        app.context.db = db;
    });

    afterAll(async () => {
        await db.close();
    });

    describe('POST /diseases', () => {

        let diseaseID;

        beforeAll(async () => {
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste"
            });

            diseaseID = insertedId;
        });

        afterAll(async () => {
            await db.collection('diseases').drop();
        });

        it('returns error for invalid disease format', async () => {
            const invalidDisease = {
                nome: 'should be "name"'
            };

            const resp = await mockRequest.post('/diseases').send(invalidDisease);

            expect(resp.body.error).toBeDefined();
        });

        it('returns error for invalid disease name', async () => {
            const invalidDisease = {
                name: '123#$ invalid name'
            };

            const resp = await mockRequest.post('/diseases').send(invalidDisease);

            expect(resp.body.error).toBeDefined();
        });

        it('returns error for repeated disease', async () => {
            const invalidDisease = {
                name: 'teste'
            };

            const resp = await mockRequest.post('/diseases').send(invalidDisease);

            expect(resp.body.error).toBeDefined();
        });

        it('returns disease for valid input', async () => {
            const validDisease = {
                name: 'nome'
            };

            const resp = await mockRequest.post('/diseases').send(validDisease);

            expect(resp.body.error).toBeUndefined();
            expect(resp.body.disease).toBeDefined();
        });


    });

    describe('GET /diseases', () => {
        beforeEach(async () => {
            await db.collection('diseases').insertOne({
                name: "teste"
            });
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('returns all diseases in DB', async () => {
            //TODO : fix to verify diseases returned are correct
            //const insertedDiseases = await db.collection('diseases').find({}).toArray();
            //expect(allDiseasesResp.body.diseases).toEqual(insertedDiseases);

            const allDiseasesResp = await mockRequest.get('/diseases');


            expect(allDiseasesResp.body.diseases).toBeDefined();
        });
    });

    describe('GET /diseases/:id', () => {
        let diseaseID;

        beforeEach(async () => {
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste"
            });
            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('db is correctly passed to the context', () => {
            expect(app.context.db).toBeDefined();
        });

        it('returns error for invalid id', async () => {
            const id = `FF690AFF690A`;
            const resp = await mockRequest.get(`/diseases/${id}`);

            expect(resp.body.error).toBeDefined();
        });

        it('returns a disease when valid id is provided', async () => {
            const resp = await mockRequest.get(`/diseases/${diseaseID}`);

            expect(resp.body.error).toBeUndefined();
            expect(resp.body.disease).toBeDefined();
        });
    });

    describe('PUT /diseases/:id', () => {
        let diseaseID;

        beforeEach(async () => {
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste"
            });
            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });


        it('returns error for invalid id', async () => {
            const invalidDisease = {
                name: 'nao existe',
            };
            const id = `121212121212`;

            const res = await mockRequest.put(`/diseases/${id}`).send(invalidDisease);

            expect(res.body.error).toBeDefined();
        });

        it('returns disease for valid id', async () => {
            const validDisease = {
                name: 'mudou',
            };

            const res = await mockRequest.put(`/diseases/${diseaseID}`).send(validDisease);

            expect(res.body.error).toBeUndefined();
            expect(res.body.disease).toBeDefined();
        });


        it('change persists in db', async () => {
            const validDisease = {
                name: 'mudou'
            };

            await mockRequest.put(`/diseases/${diseaseID}`).send(validDisease);

            const retrieved = await db.collection('diseases').find({ _id: ObjectId(diseaseID) }).toArray();

            expect(retrieved[0].name).toEqual("mudou");
        });
    });

    describe('DELETE /diseases/:id', () => {
        let diseaseID;

        beforeEach(async () => {
            const { insertedId } = await db.collection('diseases').insertOne({
                name: 'removable'
            });

            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('db is correctly passed to the context', () => {
            expect(app.context.db).toBeDefined();
        });

        it('returns error for invalid id', async () => {
            const id = `FF690AFF690A`;
            const resp = await mockRequest.delete(`/diseases/${id}`);

            expect(resp.body.error).toBeDefined();
        });

        it('returns disease when valid id is provided', async () => {
            const resp = await mockRequest.delete(`/diseases/${diseaseID}`);

            expect(resp.body.error).toBeUndefined();
            expect(resp.body.disease).toBeDefined();
        });

        it('change persist in db', async () => {
            await mockRequest.delete(`/diseases/${diseaseID}`);

            const retrieved = await db.collection('diseases').find({}).toArray();

            expect(retrieved).toEqual([]);
        });
    });

    describe('GET /diseases/diseaseID/actions/actionID', () => {
        let diseaseID;
        let actionID;

        beforeEach(async () => {
            actionID = ObjectId();
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste",
                actions: [
                    {
                        _id: actionID,
                        name: 'action',
                        disclaimer: 'disclaimer',
                        wikiID: '12345',
                        targetProfiles: [
                            {
                                sex: 'masculino',
                                minAge: 55,
                                maxAge: 75,
                            },
                        ]
                    }
                ]
            });
            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('db is correctly passed to the context', () => {
            expect(app.context.db).toBeDefined();
        });

        it('returns error for invalid diseaseID', async () => {
            const id = `FF690AFF690A`;
            const resp = await mockRequest.get(`/diseases/${id}/actions/${actionID}`);

            expect(resp.body.error).toBeDefined();
        });

        it('returns error for invalid actionID', async () => {
            const id = `FF690AFF690A`;
            const resp = await mockRequest.get(`/diseases/${diseaseID}/actions/${id}`);

            expect(resp.body.error).toBeDefined();
        });

        it('returns an action when valid id is provided', async () => {
            const resp = await mockRequest.get(`/diseases/${diseaseID}/actions/${actionID}`);

            expect(resp.body.error).toBeUndefined();
            expect(resp.body.action).toBeDefined();
        });


    });

    describe('POST /diseases/:id/actions', () => {
        let diseaseID;

        beforeEach(async () => {
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste",
                actions: [
                    {
                        name: 'action',
                        disclaimer: 'disclaimer',
                        wikiID: '12345',
                        targetProfiles: [
                            {
                                sex: 'masculino',
                                minAge: 55,
                                maxAge: 75,
                            },
                        ]
                    }
                ]
            });
            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('db is correctly passed to the context', async () => {
            expect(app.context.db).toBeDefined();
        });

        it('returns error for invalid id', async () => {
            const invalidId = `111213141516`;
            const action = {
                name: 'new action',
                disclaimer: 'disclaimer',
                wikiID: '55555',
                targetProfiles: [
                    {
                        sex: 'feminino',
                        minAge: 25,
                        maxAge: 75,
                    },
                ]
            }

            const resp = await mockRequest
                .post(`/diseases/${invalidId}/actions`).send(action);

            expect(resp.body.error).toBeDefined();
        });

        it('returns error for invalid action format', async () => {
            const noName = await mockRequest
                .post(`/diseases/${diseaseID}/actions`).send({
                    disclaimer: 'disclaimer',
                    wikiID: '55555',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });
            const noWikiForm = await mockRequest
                .post(`/diseases/${diseaseID}/actions`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });
            const noTargetProfiles = await mockRequest
                .post(`/diseases/${diseaseID}/actions`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    wikiID: '55555',
                });

            const WikiAndForm = await mockRequest
                .post(`/diseases/${diseaseID}/actions`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    formName: 'form name',
                    wikiID: '55555',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });

            expect(noName.body.error).toBeDefined();
            expect(noWikiForm.body.error).toBeDefined();
            expect(noTargetProfiles.body.error).toBeDefined();
            expect(WikiAndForm.body.error).toBeDefined();
        });

        it('returns a disease for correct parameters', async () => {
            const action = {
                name: 'new action',
                disclaimer: 'disclaimer',
                wikiID: '55555',
                targetProfiles: [
                    {
                        sex: 'feminino',
                        minAge: 25,
                        maxAge: 75,
                    },
                ]
            }

            const resp = await mockRequest
                .post(`/diseases/${diseaseID}/actions/`).send(action);

            expect(resp.body.error).toBeUndefined();
            expect(resp.body.disease).toBeDefined();
        });
    });

    describe('PUT /diseases/:id/actions/:aid', () => {
        let diseaseID;
        let actionID;

        beforeEach(async () => {
            actionID = ObjectId();
            const { insertedId } = await db.collection('diseases').insertOne({
                name: "teste",
                actions: [
                    {
                        _id: actionID,
                        name: 'action',
                        disclaimer: 'disclaimer',
                        wikiID: '12345',
                        targetProfiles: [
                            {
                                sex: 'masculino',
                                minAge: 55,
                                maxAge: 75,
                            },
                        ]
                    }
                ]
            });
            diseaseID = insertedId;
        });

        afterEach(async () => {
            await db.collection('diseases').drop();
        });

        it('db is correctly passed to the context', async () => {
            expect(app.context.db).toBeDefined();
        });

        it('returns error for invalid id', async () => {
            const invalidId = `111213141516`;
            const action = {
                name: 'new action',
                disclaimer: 'disclaimer',
                wikiID: '55555',
                targetProfiles: [
                    {
                        sex: 'feminino',
                        minAge: 25,
                        maxAge: 75,
                    },
                ]
            }

            const wrongDisId = await mockRequest
                .put(`/diseases/${invalidId}/actions/${actionID}`).send(action);

            const wrongActId = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${invalidId}`).send(action);

            expect(wrongDisId.body.error).toBeDefined();
            expect(wrongActId.body.error).toBeDefined();
        });

        it('returns error for invalid action format', async () => {
            const noName = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${actionID}`).send({
                    disclaimer: 'disclaimer',
                    wikiID: '55555',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });
            const noWikiForm = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${actionID}`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });
            const noTargetProfiles = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${actionID}`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    wikiID: '55555',
                });

            const WikiAndForm = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${actionID}`).send({
                    name: 'new action',
                    disclaimer: 'disclaimer',
                    formName: 'form name',
                    wikiID: '55555',
                    targetProfiles: [
                        {
                            sex: 'feminino',
                            minAge: 25,
                            maxAge: 75,
                        },
                    ]
                });

            expect(noName.body.error).toBeDefined();
            expect(noWikiForm.body.error).toBeDefined();
            expect(noTargetProfiles.body.error).toBeDefined();
            expect(WikiAndForm.body.error).toBeDefined();
        });

        it('returns a disease for correct parameters', async () => {
            const action = {
                name: 'new action',
                disclaimer: 'disclaimer',
                wikiID: '55555',
                targetProfiles: [
                    {
                        sex: 'feminino',
                        minAge: 25,
                        maxAge: 75,
                    },
                ]
            }

            const resp = await mockRequest
                .put(`/diseases/${diseaseID}/actions/${actionID}`).send(action);


            expect(resp.body.error).toBeUndefined();
            expect(resp.body.disease).toBeDefined();
        });
    });
});
