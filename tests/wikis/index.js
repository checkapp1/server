const request = require('supertest');

const { app } = require('../../src/app');
const { setupDB } = require('../../src/db/index');

describe('/wiki routes', () => {
  const mockRequest = request(app.callback());
  let db;

  beforeAll(async () => {
    db = await setupDB('test');
    app.context.db = db;
  });

  afterAll(async () => {
    await db.close();
  });

  describe('GET /wikis', () => {
    beforeEach(async () => {
      await db.collection('wikis').insertOne({
        name: 'Auto Wiki',
        tabs: [
          { name: 'Foo Tab 1', body: 'foo body text 1' },
          { name: 'Foo Tab 2', body: 'foo body text 2' },
        ],
      });
    });

    it('should return a non-empty array', async () => {
      const {
        body: { wikis },
      } = await mockRequest.get('/wikis');

      expect(wikis).toBeDefined();
      expect(wikis.length).toBeGreaterThan(0);
    });
  });

  describe('GET /wikis/:id', () => {
    let wikiID;

    beforeEach(async () => {
      const { insertedId } = await db.collection('wikis').insertOne({
        name: 'Auto Wiki',
        tabs: [
          { name: 'Foo Tab 1', body: 'foo body text 1' },
          { name: 'Foo Tab 2', body: 'foo body text 2' },
        ],
      });

      wikiID = insertedId;
    });

    afterEach(async () => {
      await db.collection('wikis').drop();
    });

    it('db is correctly passed to the context', () => {
      expect(app.context.db).toBeDefined();
    });

    it('returns error for invalid id', async () => {
      const id = `FF690AFF690A`;
      const resp = await mockRequest.get(`/wikis/${id}`);

      expect(resp.body.error).toBeDefined();
    });

    it('returns a wiki when valid id is provided', async () => {
      const resp = await mockRequest.get(`/wikis/${wikiID}`);

      expect(resp.body.error).toBeUndefined();
      expect(resp.body.wiki).toBeDefined();
    });
  });

  describe('POST /wikis', () => {
    it('returns error for invalid wiki', async () => {
      const invalidWiki = {
        tabs: [{ name: 'foo', body: '"content" should be "wiki' }],
      };

      const resp = await mockRequest.post('/wikis').send(invalidWiki);

      expect(resp.body.error).toBeDefined();
    });

    it('returns a wiki for valid input', async () => {
      const validWiki = {
        name: 'Valid Wiki',
        tabs: [{ name: 'foo', body: '"content" should be "wiki' }],
      };

      const resp = await mockRequest.post('/wikis').send(validWiki);

      expect(resp.body.error).toBeUndefined();
      expect(resp.body.wiki).toBeDefined();
    });
  });

  describe('PUT /wikis/:id', () => {
    const refWiki = {
      name: 'Auto Wiki',
      tabs: [
        { name: 'Foo Tab 1', body: 'foo body text 1' },
        { name: 'Foo Tab 2', body: 'foo body text 2' },
      ],
    };
    let wikiID;

    beforeEach(async () => {
      await db.collection('wikis').findOneAndDelete({});
      const { insertedId } = await db.collection('wikis').insertOne(refWiki);

      wikiID = insertedId;
    });

    it('returns an error when there is no wiki', async () => {
      const { body, status } = await mockRequest
        .put('/wikis/231802112123')
        .send({});

      expect(status).toEqual(403);
      expect(body.error).toBeDefined();
    });

    it('returns an updated version if given wiki exists', async () => {
      const name = 'New name';
      const { body } = await mockRequest.put(`/wikis/${wikiID}`).send({
        ...refWiki,
        name,
      });

      expect(body.wiki).toBeDefined();
      expect(body.wiki.name).toEqual(name);
    });
  });

  describe('DELETE /wikis/:id', () => {
    const refWiki = {
      name: 'Auto Wiki',
      tabs: [
        { name: 'Foo Tab 1', body: 'foo body text 1' },
        { name: 'Foo Tab 2', body: 'foo body text 2' },
      ],
    };
    let wikiID;

    beforeEach(async () => {
      await db.collection('wikis').deleteMany({});
      const { insertedId } = await db.collection('wikis').insertOne(refWiki);

      wikiID = insertedId;
    });

    it('returns an error if invalid id provided', async () => {
      const { status } = await mockRequest.delete('/wikis/123456789123');

      expect(status).toEqual(403);
    });

    it('deletes wiki when correct id is provided', async () => {
      const { body, status } = await mockRequest.delete(`/wikis/${wikiID}`);

      expect(status).toEqual(200);
      expect(body.wiki.name).toEqual(refWiki.name);
    });
  });
});
